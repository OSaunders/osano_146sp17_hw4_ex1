/**
 * @author osano
 * @version 4/12/2017 2:29am
 */
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import java.awt.GridLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CalculatorFrame extends JFrame {

	private JPanel contentPane;
	private JTextField lcdJTextField;
	private JButton btnNewButton;
	private JButton btnNewButton_1;
	private JButton btnNewButton_2;
	private JButton btnNewButton_3;
	private JButton btnNewButton_4;
	private JButton btnNewButton_5;
	private JButton btnNewButton_6;
	private JButton btnNewButton_7;
	private JButton btnNewButton_8;
	private JButton btnNewButton_9;
	private JButton btnNewButton_10;
	private JButton btnNewButton_11;
	private JButton btnNewButton_12;
	private JButton btnNewButton_13;
	private JButton btnNewButton_14;
	private JButton btnNewButton_15;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CalculatorFrame frame = new CalculatorFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CalculatorFrame() {
		setTitle("Calculator");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		lcdJTextField = new JTextField();
		contentPane.add(lcdJTextField, BorderLayout.NORTH);
		lcdJTextField.setColumns(10);
		StringBuilder lcdBuffer = new StringBuilder();
		
		JPanel keypadJPanel = new JPanel();
		contentPane.add(keypadJPanel, BorderLayout.CENTER);
		keypadJPanel.setLayout(new GridLayout(4, 4, 5, 5));
		
		btnNewButton_1 = new JButton("7");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lcdJTextField.setText("7");
			}
		});
		keypadJPanel.add(btnNewButton_1);
		
		btnNewButton_4 = new JButton("8");
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lcdJTextField.setText("8");
			}
		});
		keypadJPanel.add(btnNewButton_4);
		
		btnNewButton_2 = new JButton("9");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lcdJTextField.setText("9");
			}
		});
		keypadJPanel.add(btnNewButton_2);
		
		btnNewButton_3 = new JButton("/");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lcdJTextField.setText("/");
			}
		});
		keypadJPanel.add(btnNewButton_3);
		
		btnNewButton_6 = new JButton("4");
		btnNewButton_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lcdJTextField.setText("4");
			}
		});
		keypadJPanel.add(btnNewButton_6);
		
		btnNewButton_5 = new JButton("5");
		btnNewButton_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lcdJTextField.setText("5");
			}
		});
		keypadJPanel.add(btnNewButton_5);
		
		btnNewButton_12 = new JButton("6");
		btnNewButton_12.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lcdJTextField.setText("6");
			}
		});
		keypadJPanel.add(btnNewButton_12);
		
		btnNewButton_9 = new JButton("*");
		btnNewButton_9.addActionListener(e -> lcdJTextField.setText("*"));
		keypadJPanel.add(btnNewButton_9);
		
		btnNewButton_8 = new JButton("1");
		btnNewButton_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lcdJTextField.setText("1");
			}
		});
		keypadJPanel.add(btnNewButton_8);
		
		btnNewButton = new JButton("2");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lcdJTextField.setText("2");
			}
		});
		keypadJPanel.add(btnNewButton);
		
		btnNewButton_7 = new JButton("3");
		btnNewButton_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lcdJTextField.setText("3");
			}
		});
		keypadJPanel.add(btnNewButton_7);
		
		btnNewButton_10 = new JButton("-");
		btnNewButton_10.addActionListener(e -> lcdJTextField.setText("-"));
		keypadJPanel.add(btnNewButton_10);
		
		btnNewButton_11 = new JButton("0");
		btnNewButton_11.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lcdJTextField.setText("0");
			}
		});
		keypadJPanel.add(btnNewButton_11);
		
		btnNewButton_13 = new JButton(".");
		btnNewButton_13.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lcdJTextField.setText(".");
			}
		});
		keypadJPanel.add(btnNewButton_13);
		
		btnNewButton_14 = new JButton("=");
		btnNewButton_14.addActionListener(e -> lcdJTextField.setText("="));
		keypadJPanel.add(btnNewButton_14);
		
		btnNewButton_15 = new JButton("+");
		btnNewButton_15.addActionListener(e -> lcdJTextField.setText("+"));
		keypadJPanel.add(btnNewButton_15);
	}//end constructor CalculatorFrame

}//end class CalculatorFrame
